@S00004
Feature: UK ProveId - Multiple E-mails mappings
		 
    Scenario: Mapping multiple Emails
    Given A Valid CrossCore User
    And input the json file S00004_GBR_Multiple_Emails.json contained following values
    | COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
    | GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
 		When The search is submitted
 		Then The system must map multiple contacts.emails
    |John.Smith@experian.com 	| 
    |John.Smith2@experian.com | 
