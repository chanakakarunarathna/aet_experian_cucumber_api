@S00002
Feature: UK ProveId - Single Address mappings

		  Scenario: Crosscore 'addresses' mapping with Proveid 'addresses' 
			Given A Valid CrossCore User
	  	And input the json file S00002_GBR_Single_Address.json contained following values 
	  	|TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH   |GENDER 	|BUILDING_NUMBER	|STREET			|POSTTOWN					|COUNTRY	|POSTAL		|COUNTRY_CODE|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      |M				|345							|Blea Beck	|Askam-In-Furness	|Cumbria	|LA16 7DG	|GBR				 |
			When The search is submitted
			Then The system must map following sections
			|CROSSCORE_MAPPING													|PROVEID_MAPPING							|
			|contacts.addresses.buildingNumber					|Addresses.Address.Premise		|
			|contacts.addresses.street									|Addresses.Address.Street			|
			|contacts.addresses.postTown								|Addresses.Address.PostTown		|
			|contacts.addresses.county									|Addresses.Address.Region			|
			|contacts.addresses.postal									|Addresses.Address.Postcode		|
			|contacts.addresses.countryCode							|Addresses.Address.CountryCode|
			
		