@S00003
Feature: UK ProveId - Multiple Telephone numbers mappings
			Scenario: Mapping multiple telephone numbers
	    Given A Valid CrossCore User
	    And input the json file S00003_GBR_Multiple_Telephone.json contained following values
	    |TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH   |GENDER 	|BUILDING_NUMBER	|STREET			|POSTTOWN					|COUNTRY	|POSTAL		|COUNTRY_CODE|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      |M				|345							|Blea Beck	|Askam-In-Furness	|Cumbria	|LA16 7DG	|GBR				 |
	    When The search is submitted
	    Then The system must map multiple contacts.telephones.number
	    |01234567890|
      |01234567891|
      
      
