@S00011
Feature: UK ProveId - Control option mapping
			
			Scenario: Crosscore 'option' mapping with Proveid 'SearchOptions'
			Given A Valid CrossCore User
	  	And input the json file S00011_GBR_Control_Options.json contained following values 
  		|TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH   |GENDER 	|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      |M				|
			When The search is submitted
			Then The system must map following sections
			|CROSSCORE_MAPPING													|PROVEID_MAPPING						|
			|clientReferenceId													|YourReference							|
			|control.option.TARGET_COUNTRY							|CountryCode								|
			|control.option.DECISION_CODE								|SearchOptions.DecisionCode	|
			|control.option.PRODUCT_OPTION							|SearchOptions.ProductCode	|				 		