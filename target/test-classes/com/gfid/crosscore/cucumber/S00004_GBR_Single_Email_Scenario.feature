@S00004
Feature: UK ProveId - Single E-mail mapping
		 
    Scenario: Mapping single e-mail
    Given A Valid CrossCore User
    And input the json file S00004_GBR_Single_Email.json contained following values
    | COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
    | GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC  |
 		When The search is submitted
    Then The system must map following sections
    |CROSSCORE_MAPPING          |PROVEID_MAPPING 	|
    |contacts.emails.email      |Emails.Email			|
