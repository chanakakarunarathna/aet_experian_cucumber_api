@S00009
Feature: UK ProveId - Single card types mapping
    
    Scenario: Mapping American Express Card Type
		Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_AMX.json contained following values
		| COUNTRY_CODE 	| FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
		| GBR         	| SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
		When The search is submitted
		Then The system must map following cards.brand values
		|CROSSCORE_VALUE		|PROVEID_VALUE		|
		|AMX								|American Express |
		             
					 
		Scenario: Mapping Visa Card Type
		Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_VIS.JSON contained following values
		| COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
		| GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
		When The search is submitted
		Then The system must map following cards.brand values
		|CROSSCORE_VALUE	|PROVEID_VALUE				|
		|VIS  						|Visa 								|
		                
						
		Scenario: Mapping MasterCard Card Type
		Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_MCD.JSON contained following values
		| COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
		| GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
		When The search is submitted
		Then The system must map following cards.brand values
		|CROSSCORE_VALUE	|PROVEID_VALUE		|
		|MCD   						|MasterCard 			|
		                
		Scenario: Mapping Diners Club Card Type
		Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_DIN.json contained following values
		| COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
		| GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
		When The search is submitted
		Then The system must map following cards.brand values
		|CROSSCORE_VALUE	|PROVEID_VALUE		|
		|DIN      				|Diners Club  		|
		 
						
		Scenario:Mapping JCB Card Type
		Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_JCB.JSON contained following values
		| COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
		| GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
		When The search is submitted
		Then The system must map following cards.brand values
		|CROSSCORE_VALUE	|PROVEID_VALUE				|
		|JCB    					|JCB									|
		                
						
		Scenario:Mapping International Maestro Card Type
		Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_MST.json contained following values
		| COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
		| GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
		When The search is submitted
		Then The system must map following cards.brand values
		|CROSSCORE_VALUE				|PROVEID_VALUE	|
		|MST  									|International Maestro|
