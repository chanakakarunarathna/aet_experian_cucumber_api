$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("com/gfid/crosscore/cucumber/S00001_GBR_Single_Person_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single Person mappings",
  "description": "",
  "id": "uk-proveid---single-person-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00001"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Crosscore \u0027clientReferenceId\u0027 mapping with Proveid \u0027YourReference\u0027",
  "description": "",
  "id": "uk-proveid---single-person-mappings;crosscore-\u0027clientreferenceid\u0027-mapping-with-proveid-\u0027yourreference\u0027",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00001_GBR_Single_Person.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "clientReferenceId",
        "YourReference"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 288845021,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00001_GBR_Single_Person.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 2864680,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 6800739759,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 6089531,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Crosscore \u0027person\u0027 mapping with Proveid \u0027person\u0027",
  "description": "",
  "id": "uk-proveid---single-person-mappings;crosscore-\u0027person\u0027-mapping-with-proveid-\u0027person\u0027",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "input the json file S00001_GBR_Single_Person.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 18
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 19
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 22
    },
    {
      "cells": [
        "contacts.person.personDetails.dateOfBirth",
        "Person.DateOfBirth"
      ],
      "line": 23
    },
    {
      "cells": [
        "contacts.person.personDetails.gender",
        "Person.Gender"
      ],
      "line": 24
    },
    {
      "cells": [
        "contacts.person.names.title",
        "Person.Name.Title"
      ],
      "line": 25
    },
    {
      "cells": [
        "contacts.person.names.firstName",
        "Person.Name.Forename"
      ],
      "line": 26
    },
    {
      "cells": [
        "contacts.person.names.middleNames",
        "Person.Name.OtherNames"
      ],
      "line": 27
    },
    {
      "cells": [
        "contacts.person.names.surName",
        "Person.Name.Surname"
      ],
      "line": 28
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 503975,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00001_GBR_Single_Person.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 261556,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2788384418,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 851505,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00002_GBR_Single_Address_Sceanrio.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single Address mappings",
  "description": "",
  "id": "uk-proveid---single-address-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00002"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Crosscore \u0027addresses\u0027 mapping with Proveid \u0027addresses\u0027",
  "description": "",
  "id": "uk-proveid---single-address-mappings;crosscore-\u0027addresses\u0027-mapping-with-proveid-\u0027addresses\u0027",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00002_GBR_Single_Address.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.addresses.buildingNumber",
        "Addresses.Address.Premise"
      ],
      "line": 12
    },
    {
      "cells": [
        "contacts.addresses.street",
        "Addresses.Address.Street"
      ],
      "line": 13
    },
    {
      "cells": [
        "contacts.addresses.postTown",
        "Addresses.Address.PostTown"
      ],
      "line": 14
    },
    {
      "cells": [
        "contacts.addresses.county",
        "Addresses.Address.Region"
      ],
      "line": 15
    },
    {
      "cells": [
        "contacts.addresses.postal",
        "Addresses.Address.Postcode"
      ],
      "line": 16
    },
    {
      "cells": [
        "contacts.addresses.countryCode",
        "Addresses.Address.CountryCode"
      ],
      "line": 17
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 593093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00002_GBR_Single_Address.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 283975,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2943006164,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 793606,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00003_GBR_Multiple_Telephone_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Multiple Telephone numbers mappings",
  "description": "",
  "id": "uk-proveid---multiple-telephone-numbers-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00003"
    }
  ]
});
formatter.scenario({
  "line": 3,
  "name": "Mapping multiple telephone numbers",
  "description": "",
  "id": "uk-proveid---multiple-telephone-numbers-mappings;mapping-multiple-telephone-numbers",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "input the json file S00003_GBR_Multiple_Telephone.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 6
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 7
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The system must map multiple contacts.telephones.number",
  "rows": [
    {
      "cells": [
        "01234567890"
      ],
      "line": 10
    },
    {
      "cells": [
        "01234567891"
      ],
      "line": 11
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 629130,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00003_GBR_Multiple_Telephone.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 419886,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2998420889,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contacts.telephones.number",
      "offset": 29
    }
  ],
  "location": "Execution.The_system_must_map_multiple_values(String,String\u003e)"
});
formatter.result({
  "duration": 811136,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00003_GBR_Single_Telephone_Sceanrio.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single telephone details mappings",
  "description": "",
  "id": "uk-proveid---single-telephone-details-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00003"
    }
  ]
});
formatter.scenario({
  "line": 3,
  "name": "Mapping single telephone details",
  "description": "",
  "id": "uk-proveid---single-telephone-details-mappings;mapping-single-telephone-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "input the json file S00003_GBR_Single_Telephone.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 6
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 7
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 10
    },
    {
      "cells": [
        "contacts.telephones.phoneIdentifier",
        "Telephones.Telephone Type"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.telephones.number",
        "Telephones.Number"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 944115,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00003_GBR_Single_Telephone.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 287676,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2564024408,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 1040426,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00003_GBR_Single_Telephone_Type_Sceanrio.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single Telephone types mappings",
  "description": "",
  "id": "uk-proveid---single-telephone-types-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00003"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping Home Telephone Type",
  "description": "",
  "id": "uk-proveid---single-telephone-types-mappings;mapping-home-telephone-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00003_GBR_Single_Telephone_Type_HOME.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following contacts.telephones.phoneIdentifier values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 11
    },
    {
      "cells": [
        "HOME",
        "H"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 795911,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00003_GBR_Single_Telephone_Type_HOME.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 288025,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2727440080,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contacts.telephones.phoneIdentifier",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 1828515,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Mapping WORK Telephone Type",
  "description": "",
  "id": "uk-proveid---single-telephone-types-mappings;mapping-work-telephone-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 16,
  "name": "input the json file S00003_GBR_Single_Telephone_Type_WORK.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 17
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "The system must map following contacts.telephones.phoneIdentifier values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 21
    },
    {
      "cells": [
        "WORK",
        "W"
      ],
      "line": 22
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 1165861,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00003_GBR_Single_Telephone_Type_WORK.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 300667,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2811061951,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contacts.telephones.phoneIdentifier",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 399562,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Mapping LANDLINE Telephone Type",
  "description": "",
  "id": "uk-proveid---single-telephone-types-mappings;mapping-landline-telephone-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "input the json file S00003_GBR_Single_Telephone_Type_LANDLINE.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 27
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 28
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "The system must map following contacts.telephones.phoneIdentifier values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 31
    },
    {
      "cells": [
        "LANDLINE",
        "L"
      ],
      "line": 32
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 172927,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00003_GBR_Single_Telephone_Type_LANDLINE.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 180190,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2440786050,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contacts.telephones.phoneIdentifier",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 1819575,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Mapping MOBILE Telephone Type",
  "description": "",
  "id": "uk-proveid---single-telephone-types-mappings;mapping-mobile-telephone-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 35,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 36,
  "name": "input the json file S00003_GBR_Single_Telephone_Type_MOBILE.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 37
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 38
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 39,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "The system must map following contacts.telephones.phoneIdentifier values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 41
    },
    {
      "cells": [
        "MOBILE",
        "M"
      ],
      "line": 42
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 422889,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00003_GBR_Single_Telephone_Type_MOBILE.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 274197,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2387462234,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contacts.telephones.phoneIdentifier",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 572698,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00004_GBR_Multiple_Emails_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Multiple E-mails mappings",
  "description": "",
  "id": "uk-proveid---multiple-e-mails-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00004"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping multiple Emails",
  "description": "",
  "id": "uk-proveid---multiple-e-mails-mappings;mapping-multiple-emails",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00004_GBR_Multiple_Emails.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map multiple contacts.emails",
  "rows": [
    {
      "cells": [
        "John.Smith@experian.com"
      ],
      "line": 11
    },
    {
      "cells": [
        "John.Smith2@experian.com"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 406336,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00004_GBR_Multiple_Emails.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 1439987,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2333554824,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contacts.emails",
      "offset": 29
    }
  ],
  "location": "Execution.The_system_must_map_multiple_values(String,String\u003e)"
});
formatter.result({
  "duration": 542527,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00004_GBR_Single_Email_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single E-mail mapping",
  "description": "",
  "id": "uk-proveid---single-e-mail-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00004"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping single e-mail",
  "description": "",
  "id": "uk-proveid---single-e-mail-mapping;mapping-single-e-mail",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00004_GBR_Single_Email.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.emails.email",
        "Emails.Email"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 389086,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00004_GBR_Single_Email.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 9010712,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2454454127,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 1559137,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00005_GBR_Passport_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Passport details mapping",
  "description": "",
  "id": "uk-proveid---passport-details-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00005"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping Passport details",
  "description": "",
  "id": "uk-proveid---passport-details-mapping;mapping-passport-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00005_GBR_Passport.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].documentNumber",
        "Passport.PassportNumber"
      ],
      "line": 12
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].personalNumber",
        "Passport.PersonalNumber"
      ],
      "line": 13
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].nationalityCountryCode",
        "Passport.NationalityCode"
      ],
      "line": 14
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].issueCountryCode",
        "Passport.IssuingCountryCode"
      ],
      "line": 15
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].expiresDate",
        "Passport.ExpiryDate"
      ],
      "line": 16
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].MRZLine1",
        "Passport.PassportLine1"
      ],
      "line": 17
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPASSPORT].MRZLine2",
        "Passport.PassportLine2"
      ],
      "line": 18
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 380425,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00005_GBR_Passport.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 323714,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2888497491,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 537638,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00006_GBR_DrivingLicense_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Driving License mapping",
  "description": "",
  "id": "uk-proveid---driving-license-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00006"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping Driving License number",
  "description": "",
  "id": "uk-proveid---driving-license-mapping;mapping-driving-license-number",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00006_GBR_DrivingLicense.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dDRIVER_LICENSE].documentNumber",
        "DriverLicence.DriverLicenceNumber"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 221397,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00006_GBR_DrivingLicense.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 165175,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2626633622,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 559708,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00007_GBR_IdentityDocument_PAN_Detail_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Identitiy document - PAN details mapping",
  "description": "",
  "id": "uk-proveid---identitiy-document---pan-details-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00007"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping IdentityDocument - PAN Details",
  "description": "",
  "id": "uk-proveid---identitiy-document---pan-details-mapping;mapping-identitydocument---pan-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00007_GBR_IdentityDocument_PAN_Detail.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPAN].documentType",
        "IdentityDocument.TypeOfDocument"
      ],
      "line": 12
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dPAN].documentNumber",
        "IdentityDocument.Number"
      ],
      "line": 13
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 594280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00007_GBR_IdentityDocument_PAN_Detail.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 288235,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2404295518,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 477366,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00007_GBR_IdentityDocument_SIN Details_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Identitiy document - SIN details mapping",
  "description": "",
  "id": "uk-proveid---identitiy-document---sin-details-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00007"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping IdentityDocument - SIN Details",
  "description": "",
  "id": "uk-proveid---identitiy-document---sin-details-mapping;mapping-identitydocument---sin-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00007_GBR_IdentityDocument_SIN Details.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dSIN].documentType",
        "IdentityDocument.TypeOfDocument"
      ],
      "line": 12
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dSIN].documentNumber",
        "IdentityDocument.Number"
      ],
      "line": 13
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 397118,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00007_GBR_IdentityDocument_SIN Details.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 182845,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2368700282,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 423797,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00007_GBR_IdentityDocument_SSN Details_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Identitiy document - SSN details mapping",
  "description": "",
  "id": "uk-proveid---identitiy-document---ssn-details-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00007"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping IdentityDocument - SSN Details",
  "description": "",
  "id": "uk-proveid---identitiy-document---ssn-details-mapping;mapping-identitydocument---ssn-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00007_GBR_IdentityDocument_SSN_Details.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dSSN].documentType",
        "IdentityDocument.TypeOfDocument"
      ],
      "line": 12
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dSSN].documentNumber",
        "IdentityDocument.Number"
      ],
      "line": 13
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 1013257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00007_GBR_IdentityDocument_SSN_Details.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 557473,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2416619571,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 2233105,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00007_GBR_IdentityDocument_VoterID_Detail_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Identitiy document - VoterID details mapping",
  "description": "",
  "id": "uk-proveid---identitiy-document---voterid-details-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00007"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping IdentityDocument - VoterID Details",
  "description": "",
  "id": "uk-proveid---identitiy-document---voterid-details-mapping;mapping-identitydocument---voterid-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00007_GBR_IdentityDocument_VoterID_Detail.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dVoterID].documentType",
        "IdentityDocument.Number"
      ],
      "line": 12
    },
    {
      "cells": [
        "contacts.identityDocuments.@[documentType\u003dVoterID].documentNumber",
        "IdentityDocument.Number"
      ],
      "line": 13
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 335169,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00007_GBR_IdentityDocument_VoterID_Detail.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 262812,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2407389277,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 380216,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00008_GBR_Single_Card_Transaction_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single card transaction mapping",
  "description": "",
  "id": "uk-proveid---single-card-transaction-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00008"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping card transaction details",
  "description": "",
  "id": "uk-proveid---single-card-transaction-mapping;mapping-card-transaction-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00008_GBR_Single_Card_Transaction.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER",
        "BUILDING_NUMBER",
        "STREET",
        "POSTTOWN",
        "COUNTRY",
        "POSTAL",
        "COUNTRY_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M",
        "345",
        "Blea Beck",
        "Askam-In-Furness",
        "Cumbria",
        "LA16 7DG",
        "GBR"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "application.originalRequestTime",
        "Orders.Order.Payment.Cards.Card.Transaction.TransactionDate"
      ],
      "line": 12
    },
    {
      "cells": [
        "transactions.cashValue.amount",
        "Orders.Order.Payment.Cards.Card.Transaction.TransactionAmount.Amount"
      ],
      "line": 13
    },
    {
      "cells": [
        "transactions.cashValue.currencyCode",
        "Orders.Order.Payment.Cards.Card.Transaction.TransactionAmount.Currency"
      ],
      "line": 14
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 172298,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00008_GBR_Single_Card_Transaction.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 472407,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2587828202,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 813651,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00009_GBR_Single_Card_Security_Code_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single card mappings",
  "description": "",
  "id": "uk-proveid---single-card-mappings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00009"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping clear_card number - verify card number is in correct format",
  "description": "",
  "id": "uk-proveid---single-card-mappings;mapping-clear-card-number---verify-card-number-is-in-correct-format",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00009_GBR_Single_Card_Security_Code.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "cards.clearCardNumber",
        "Orders.Order.Payment.Cards.Card.Number"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 369321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_Code.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 150228,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2426825337,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 588413,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Mapping all card details",
  "description": "",
  "id": "uk-proveid---single-card-mappings;mapping-all-card-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "input the json file S00009_GBR_Single_Card_Security_Code.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 18
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 19
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 22
    },
    {
      "cells": [
        "cards.brand",
        "Orders.Order.Payment.Cards.Type"
      ],
      "line": 23
    },
    {
      "cells": [
        "cards.cardBinNumber",
        "Orders.Order.Payment.Cards.BINNumber"
      ],
      "line": 24
    },
    {
      "cells": [
        "cards.clearCardNumber",
        "Orders.Order.Payment.Cards.Number"
      ],
      "line": 25
    },
    {
      "cells": [
        "cards.cardLast4Digits",
        "Orders.Order.Payment.Cards.CardLast4Digits"
      ],
      "line": 26
    },
    {
      "cells": [
        "cards.cardHolderName",
        "Orders.Order.Payment.Cards.Name"
      ],
      "line": 27
    },
    {
      "cells": [
        "cards.expireDate",
        "Orders.Order.Payment.Cards.ExpiresEnd"
      ],
      "line": 28
    },
    {
      "cells": [
        "cards.startDate",
        "Orders.Order.Payment.Cards.StartDate"
      ],
      "line": 29
    },
    {
      "cells": [
        "cards.issueNumber",
        "Orders.Order.Payment.Cards.IssueNumber"
      ],
      "line": 30
    },
    {
      "cells": [
        "cards.securityCode",
        "Orders.Order.Payment.Cards.SecurityCode"
      ],
      "line": 31
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 359682,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_Code.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 219511,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2648503580,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 543784,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00009_GBR_Single_Card_Security_Types_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Single card types mapping",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00009"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Mapping American Express Card Type",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping;mapping-american-express-card-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00009_GBR_Single_Card_Security_AMX.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 7
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following cards.brand values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 11
    },
    {
      "cells": [
        "AMX",
        "American Express"
      ],
      "line": 12
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 255061,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_AMX.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 176908,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 3387332748,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cards.brand",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 1419734,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Mapping Visa Card Type",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping;mapping-visa-card-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 16,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "input the json file S00009_GBR_Single_Card_Security_VIS.JSON contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 18
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 19
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "The system must map following cards.brand values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 22
    },
    {
      "cells": [
        "VIS",
        "Visa"
      ],
      "line": 23
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 232641,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_VIS.JSON",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 292146,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2745653009,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cards.brand",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 484489,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Mapping MasterCard Card Type",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping;mapping-mastercard-card-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 28,
  "name": "input the json file S00009_GBR_Single_Card_Security_MCD.JSON contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 29
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 30
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "The system must map following cards.brand values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 33
    },
    {
      "cells": [
        "MCD",
        "MasterCard"
      ],
      "line": 34
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 157562,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_MCD.JSON",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 146807,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2406806592,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cards.brand",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 380705,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Mapping Diners Club Card Type",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping;mapping-diners-club-card-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 37,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 38,
  "name": "input the json file S00009_GBR_Single_Card_Security_DIN.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 39
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 40
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "The system must map following cards.brand values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 43
    },
    {
      "cells": [
        "DIN",
        "Diners Club"
      ],
      "line": 44
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 293124,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_DIN.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 161193,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2584734722,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cards.brand",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 447822,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Mapping JCB Card Type",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping;mapping-jcb-card-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 48,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 49,
  "name": "input the json file S00009_GBR_Single_Card_Security_JCB.JSON contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 50
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 51
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "The system must map following cards.brand values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 54
    },
    {
      "cells": [
        "JCB",
        "JCB"
      ],
      "line": 55
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 479391,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_JCB.JSON",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 231105,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2494661612,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cards.brand",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 420304,
  "status": "passed"
});
formatter.scenario({
  "line": 58,
  "name": "Mapping International Maestro Card Type",
  "description": "",
  "id": "uk-proveid---single-card-types-mapping;mapping-international-maestro-card-type",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 59,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 60,
  "name": "input the json file S00009_GBR_Single_Card_Security_MST.json contained following values",
  "rows": [
    {
      "cells": [
        "COUNTRY_CODE",
        "FORENAME",
        "SURNAME",
        "PREMISE",
        "POSTCODE",
        "PRODUCT_CODE"
      ],
      "line": 61
    },
    {
      "cells": [
        "GBR",
        "SIRATH",
        "NIVER",
        "345",
        "LA16 7DG",
        "ProveID_KYC"
      ],
      "line": 62
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "The system must map following cards.brand values",
  "rows": [
    {
      "cells": [
        "CROSSCORE_VALUE",
        "PROVEID_VALUE"
      ],
      "line": 65
    },
    {
      "cells": [
        "MST",
        "International Maestro"
      ],
      "line": 66
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 180470,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00009_GBR_Single_Card_Security_MST.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 192901,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2600838375,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cards.brand",
      "offset": 30
    }
  ],
  "location": "Execution.The_system_must_map_following_values(String,DataTable)"
});
formatter.result({
  "duration": 465213,
  "status": "passed"
});
formatter.uri("com/gfid/crosscore/cucumber/S00011_GBR_Control_Option_Scenario.feature");
formatter.feature({
  "line": 2,
  "name": "UK ProveId - Control option mapping",
  "description": "",
  "id": "uk-proveid---control-option-mapping",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@S00011"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Crosscore \u0027option\u0027 mapping with Proveid \u0027SearchOptions\u0027",
  "description": "",
  "id": "uk-proveid---control-option-mapping;crosscore-\u0027option\u0027-mapping-with-proveid-\u0027searchoptions\u0027",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "A Valid CrossCore User",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "input the json file S00011_GBR_Control_Options.json contained following values",
  "rows": [
    {
      "cells": [
        "TITLE",
        "FORENAME",
        "SURNAME",
        "DATE_OF_BIRTH",
        "GENDER"
      ],
      "line": 7
    },
    {
      "cells": [
        "MR",
        "SIRATH",
        "NIVER",
        "1994-07-01",
        "M"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The search is submitted",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "The system must map following sections",
  "rows": [
    {
      "cells": [
        "CROSSCORE_MAPPING",
        "PROVEID_MAPPING"
      ],
      "line": 11
    },
    {
      "cells": [
        "clientReferenceId",
        "YourReference"
      ],
      "line": 12
    },
    {
      "cells": [
        "control.option.TARGET_COUNTRY",
        "CountryCode"
      ],
      "line": 13
    },
    {
      "cells": [
        "control.option.DECISION_CODE",
        "SearchOptions.DecisionCode"
      ],
      "line": 14
    },
    {
      "cells": [
        "control.option.PRODUCT_OPTION",
        "SearchOptions.ProductCode"
      ],
      "line": 15
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CrossCore",
      "offset": 8
    }
  ],
  "location": "Execution.aValidProductUser(String)"
});
formatter.result({
  "duration": 182635,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "S00011_GBR_Control_Options.json",
      "offset": 20
    }
  ],
  "location": "Execution.inputTheJsonFile(String,DataTable)"
});
formatter.result({
  "duration": 230127,
  "status": "passed"
});
formatter.match({
  "location": "Execution.theSearchIsSubmitted()"
});
formatter.result({
  "duration": 2442212767,
  "status": "passed"
});
formatter.match({
  "location": "Execution.The_system_must_map_successful_request(DataTable)"
});
formatter.result({
  "duration": 501950,
  "status": "passed"
});
});