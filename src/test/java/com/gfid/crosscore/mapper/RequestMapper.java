package com.gfid.crosscore.mapper;

import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonNode;

import com.gfid.crosscore.util.CucumberUtil;

import xsd.common.AddressType;
import xsd.common.CardType;
import xsd.common.EmailType;
import xsd.common.FieldType;
import xsd.common.IdentityDocumentType;
import xsd.common.OrderType;
import xsd.common.TelephoneType;
import xsd.search.SearchType;

public class RequestMapper {

	public boolean compareClientReferenceId(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		cross_mappingValue = crosscoreRootNode.path("header").get(cross_mappingField).asText();
		try {
			prove_mappingValue = proveIdRequest.getYourReference();
		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveId " + prove_mappingField);
		}

		return isEqual;
	}

	public boolean compareSinglePersonDetails(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> contactsIterator;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		contactsIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (contactsIterator.hasNext()) {
			cross_mappingValue = contactsIterator.next().get("person").get("personDetails").get(cross_mappingEndTag).asText();
		}

		try {

			if (cross_mappingEndTag.equals("dateOfBirth"))
				prove_mappingValue = proveIdRequest.getPerson().getDateOfBirth().getValue().toString();
			if (cross_mappingEndTag.equals("gender"))
				prove_mappingValue = proveIdRequest.getPerson().getGender().getValue().toString();

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}


	public boolean compareSinglePersonName(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		Iterator<JsonNode> contactsIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (contactsIterator.hasNext()) {
			JsonNode cross_namesArray = contactsIterator.next().get("person").path("names");

			for (JsonNode cross_name : cross_namesArray) {
				cross_mappingValue = cross_name.path(cross_mappingEndTag).asText();
			}
		}

		try {
			if (cross_mappingEndTag.equals("title"))
				prove_mappingValue = proveIdRequest.getPerson().getName().getTitle().getValue().toString();
			if (cross_mappingEndTag.equals("firstName"))
				prove_mappingValue = proveIdRequest.getPerson().getName().getForename().getValue().toString();
			if (cross_mappingEndTag.equals("middleNames"))
				prove_mappingValue = proveIdRequest.getPerson().getName().getOtherNames().getValue().toString();
			if (cross_mappingEndTag.equals("surName"))
				prove_mappingValue = proveIdRequest.getPerson().getName().getSurname().getValue().toString();

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;

	}

	public boolean compareSingleAddress(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		List<AddressType> prove_addressList;
		JsonNode cross_addressList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();
		prove_addressList = proveIdRequest.getAddresses().getAddress();

		while (cross_contactIterator.hasNext()) {

			cross_addressList = (JsonNode) cross_contactIterator.next().get("addresses");

			int addressCount = 0;
			for (JsonNode cross_address : cross_addressList) {
				cross_mappingValue = cross_address.get(cross_mappingEndTag).asText();

				try {
					if (cross_mappingEndTag.equals("buildingNumber")) {
						prove_mappingValue = prove_addressList
								.get(addressCount).getPremise().getValue().toString();
						break;
					}
					if (cross_mappingEndTag.equals("street")) {
						prove_mappingValue = prove_addressList
								.get(addressCount).getStreet().getValue().toString();
						break;
					}
					if (cross_mappingEndTag.equals("postTown")) {
						prove_mappingValue = prove_addressList
								.get(addressCount).getPostTown().getValue().toString();
						break;
					}
					if (cross_mappingEndTag.equals("county")) {
						prove_mappingValue = prove_addressList
								.get(addressCount).getRegion().getValue().toString();
						break;
					}
					if (cross_mappingEndTag.equals("postal")) {
						prove_mappingValue = prove_addressList
								.get(addressCount).getPostcode().getValue().toString();
						break;
					}
					if (cross_mappingEndTag.equals("countryCode")) {
						prove_mappingValue = prove_addressList
								.get(addressCount).getCountryCode().getValue().toString();
						break;
					}

				} catch (NullPointerException e) {
					throw new RequestMappingException(prove_mappingField + " node value is null");
				}

			}
		}

		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}


	public boolean compareControlOptions(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_optionIterator;
		
		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_optionIterator = crosscoreRootNode.path("payload").get("control").getElements();
		while (cross_optionIterator.hasNext()) {

			JsonNode cross_optionJson = cross_optionIterator.next();
			JsonNode cross_optionName = cross_optionJson.get("option");

			if (cross_optionName.asText().equals(cross_mappingEndTag)) {
				cross_mappingValue = cross_optionJson.get("value").asText();
				break;
			}
		}

		try {
			if (cross_mappingEndTag.equals("TARGET_COUNTRY")) {
				prove_mappingValue = proveIdRequest.getCountryCode();

			} else if (cross_mappingEndTag.equals("PRODUCT_OPTION")) {
				prove_mappingValue = proveIdRequest.getSearchOptions().getProductCode();

			} else if (cross_mappingEndTag.equals("DECISION_CODE")) {
				prove_mappingValue = proveIdRequest.getSearchOptions().getDecisionCode();

			}
		} catch (NullPointerException e) {
			throw new RequestMappingException(cross_mappingField + " node value is null");
		}
		
		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}


	public boolean compareSingleEmail(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		List<EmailType> prove_emailList;
		JsonNode cross_emailList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();
		prove_emailList = proveIdRequest.getEmails().getEmail();

		while (cross_contactIterator.hasNext()) {

			cross_emailList = (JsonNode) cross_contactIterator.next().get("emails");

			int email_count = 0;
			for (JsonNode cross_email : cross_emailList) {
				cross_mappingValue = cross_email.get(cross_mappingEndTag).asText();
				try {

					if (cross_mappingEndTag.equals("type"))
						prove_mappingValue = prove_emailList.get(email_count).getType();
					if (cross_mappingEndTag.equals("email"))
						prove_mappingValue = prove_emailList.get(email_count).getValue();

				} catch (NullPointerException e) {
					throw new RequestMappingException(prove_mappingField + " node value is null");
				}

				if (cross_mappingValue.equals(prove_mappingValue)) {
					isEqual = true;
				} else {
					throw new RequestMappingException(cross_mappingField + " doesn't match with proveid "
							+ prove_mappingField + ", mapping value is " + cross_mappingValue);
				}

				email_count++;
			}

		}

		return isEqual;
	}

	public boolean compareMultipleEmails(SearchType proveIdRequest, JsonNode crosscoreRootNode, List<String> valueList)
			throws RequestMappingException {

		boolean isEqual = false;
		List<EmailType> prov_emailList = null;

		try {
			prov_emailList = proveIdRequest.getEmails().getEmail();
		} catch (NullPointerException e) {
			throw new RequestMappingException("ProveId emailList  is null");
		}

		for (int i = 0; i < valueList.size(); i++) {

			if (prov_emailList.get(i).getValue().equals(valueList.get(i))) {
				isEqual = true;
			} else {
				throw new RequestMappingException(valueList.get(i) + "email is not found in proveid request");
			}

		}
		return isEqual;
	}

  public boolean compareSingleCard(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_cardList;
		List<OrderType> prove_orderList;
		List<CardType> prove_cardList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();
		prove_orderList = proveIdRequest.getOrders().getOrder();

		while (cross_contactIterator.hasNext()) {

			cross_cardList = (JsonNode) cross_contactIterator.next().get("cards");

			for (JsonNode cross_card : cross_cardList) {
				cross_mappingValue = cross_card.path(cross_mappingEndTag).asText();
			}

		}

		for (OrderType orderType : prove_orderList) {

			prove_cardList = orderType.getPayment().getCards().getCard();

			for (CardType prove_cardType : prove_cardList) {

				if (cross_mappingEndTag.equals("clearCardNumber")) {
					if (prove_cardType.getNumber().contains("************")) {
						prove_mappingValue = prove_cardType.getNumber();
						prove_mappingValue = prove_mappingValue.substring(12);
						cross_mappingValue = cross_mappingValue.substring(12);
					} else {
						throw new RequestMappingException("Not a valide Prove ID clearCardNumber");
					}
				}

				if (cross_mappingEndTag.equals("brand")) {

					if (prove_cardType.getType().equals("American Express")) {
						prove_mappingValue = "AMX";
					} else if (prove_cardType.getType().equals("Visa")) {
						prove_mappingValue = "VIS";
					} else if (prove_cardType.getType().equals("MasterCard")) {
						prove_mappingValue = "MCD";
					} else if (prove_cardType.getType().equals("Diners Club")) {
						prove_mappingValue = "DIN";
					} else if (prove_cardType.getType().equals("JCB")) {
						prove_mappingValue = "JCB";
					} else if (prove_cardType.getType().equals("International Maestro")) {
						prove_mappingValue = "MST";
					} else {
						throw new RequestMappingException(cross_mappingField + " doesn't match with Prove Id card type called : " + prove_cardType.getType().toString());
					}
				}
				
				if (cross_mappingEndTag.equals("expireDate")){
					prove_mappingValue = prove_cardType.getExpiresEnd();
					cross_mappingValue = CucumberUtil.convertProveidTypeExpireDate(cross_mappingValue);
				}
				
				try {
					
					if (cross_mappingEndTag.equals("cardLast4Digits"))
						prove_mappingValue = prove_cardType.getCardLast4Digits();
					if (cross_mappingEndTag.equals("cardHolderName"))
						prove_mappingValue = prove_cardType.getName();
					if (cross_mappingEndTag.equals("cardBinNumber"))
						prove_mappingValue = prove_cardType.getBINNumber();
					if (cross_mappingEndTag.equals("startDate"))
						prove_mappingValue = prove_cardType.getStartDate().toString();
					if (cross_mappingEndTag.equals("issueNumber"))
						prove_mappingValue = prove_cardType.getIssueNumber().toString();
					if (cross_mappingEndTag.equals("securityCode"))
						prove_mappingValue = prove_cardType.getIssueNumber().toString();
					if (cross_mappingEndTag.equals("securityCode"))
						prove_mappingValue = prove_cardType.getSecurityCode();
					
				} catch (NullPointerException e) {
					throw new RequestMappingException(prove_mappingField + " node value is null");
				}
			}
		}
		
		if (cross_mappingValue.equals((prove_mappingValue))) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  public boolean compareSingleTelephone(SearchType proveIdRequest, JsonNode crosscoreRootNode,
			String cross_mappingField, String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_telephoneList;
		List<TelephoneType> prove_telephoneList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_telephoneList = (JsonNode) cross_contactIterator.next().get("telephones");
			
			for (JsonNode cross_telephone : cross_telephoneList) {
				cross_mappingValue = cross_telephone.path(cross_mappingEndTag).asText();
				
				if (cross_mappingValue.equals("HOME")) {
					cross_mappingValue = "H";
				}
			}

		}

		prove_telephoneList = proveIdRequest.getTelephones().getTelephone();

		try {

			for (TelephoneType prove_telephone : prove_telephoneList) {
				if (cross_mappingEndTag.equals("phoneIdentifier")) {
					prove_mappingValue = prove_telephone.getType();
				} else if (cross_mappingEndTag.equals("number")) {
					prove_mappingValue = prove_telephone.getNumber();
				}
			}

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  public boolean compareMultiTelephone(SearchType proveIdRequest, JsonNode crosscoreRootNode,
			List<String> cucumber_telephoneList, String crossMapper) throws RequestMappingException {

		boolean isEqual = false;
		List<TelephoneType> prove_telephoneList = proveIdRequest.getTelephones().getTelephone();

		for (String cucumber_telephone : cucumber_telephoneList) {
			isEqual = false;
			for (TelephoneType prove_telephone : prove_telephoneList) {
				if (crossMapper.equals("contacts.telephones.number")) {
					if (prove_telephone.getNumber().equals(cucumber_telephone)) {
						isEqual = true;
						break;
					}
				}
			}

			if (!isEqual) {
				throw new RequestMappingException(
						crossMapper + " " + cucumber_telephone + " doesn't match with proveid telephone values");
			}

		}

		return isEqual;
	}


  public boolean compareApplication(SearchType proveIdRequest, JsonNode crosscoreRootNode,
			String cross_mappingField, String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		JsonNode cross_application;
		List<OrderType> prove_orderList;
		List<CardType> prove_cardsList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_application = crosscoreRootNode.path("payload").get("application");

		if (cross_mappingEndTag.equals("originalRequestTime")) {
			cross_mappingValue = cross_application.get(cross_mappingEndTag).asText();

			prove_orderList = proveIdRequest.getOrders().getOrder();

			try {

				for (OrderType prove_orderType : prove_orderList) {
					prove_cardsList = prove_orderType.getPayment().getCards().getCard();
					for (CardType prove_cardType : prove_cardsList) {
						prove_mappingValue = prove_cardType.getTransaction().getTransactionDate().toString();
					}
				}

			} catch (NullPointerException e) {
				throw new RequestMappingException(prove_mappingField + " node value is null");
			}

		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  
	public boolean compareSingleTransaction(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;
		Float cross_mappingFloatValue = null;
		Float prove_mappingFloatValue = null;

		Iterator<JsonNode> cross_transactionsIterator;
		List<OrderType> prove_orderList;
		List<CardType> prove_cardsList;
		
		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_transactionsIterator = crosscoreRootNode.path("payload").get("transactions").getElements();

		while (cross_transactionsIterator.hasNext()) {
			if (cross_mappingEndTag.equals("amount")) {
				cross_mappingFloatValue = Float.parseFloat(cross_transactionsIterator.next().get("cashValue").get(cross_mappingEndTag).asText());
				break;
			} else if (cross_mappingEndTag.equals("currencyCode")) {
				cross_mappingValue = cross_transactionsIterator.next().get("cashValue").get(cross_mappingEndTag).asText();
				break;
			}

		}
		try {
			prove_orderList = proveIdRequest.getOrders().getOrder();
			for (OrderType prove_orderType : prove_orderList) {
				
				prove_cardsList = prove_orderType.getPayment().getCards().getCard();
				
				for (CardType prove_cardType : prove_cardsList) {

					if (cross_mappingEndTag.equals("amount")) {
						prove_mappingFloatValue = prove_cardType.getTransaction().getTransactionAmount().getAmount();
						break;

					} else if (cross_mappingEndTag.equals("currencyCode")) {
						prove_mappingValue = prove_cardType.getTransaction().getTransactionAmount().getCurrency();
						break;
					}
				}
			}
		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else if (Float.compare(cross_mappingFloatValue,prove_mappingFloatValue) == 0) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  	public boolean compareUserDefinedFields(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_userDefinedFieldsIterator;
		Iterator<JsonNode> cross_userDefinedElementsIterator;
		List<FieldType> prove_fieldTypeList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_userDefinedFieldsIterator = crosscoreRootNode.path("payload").get("userDefinedFields").getElements();
		prove_fieldTypeList = proveIdRequest.getYourData().getFields().getField();

		while (cross_userDefinedFieldsIterator.hasNext()) {

			cross_userDefinedElementsIterator = cross_userDefinedFieldsIterator.next().get("userDefinedElements").getElements();

			while (cross_userDefinedElementsIterator.hasNext()) {
				
				cross_mappingValue = cross_userDefinedElementsIterator.next().get(cross_mappingEndTag).asText();

				for (FieldType prove_fieldType : prove_fieldTypeList) {
					try {

						if (cross_mappingEndTag.equals("fieldName"))
							prove_mappingValue = prove_fieldType.getName();
						if (cross_mappingEndTag.equals("fieldValue"))
							prove_mappingValue = prove_fieldType.getValue();

					} catch (NullPointerException e) {
						throw new RequestMappingException(prove_mappingValue + " node value is null");
					}
				}

			}

		}

		if (cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

	public boolean compareIdentityPassportDocument(
			SearchType proveIdRequest, JsonNode crosscoreRootNode,
			String cross_mappingField, String prove_mappingField)
			throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_identityDocumentsArray;
		
		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_identityDocumentsArray = (JsonNode) cross_contactIterator.next().get("identityDocuments");
			for (JsonNode cross_identityDocument : cross_identityDocumentsArray) {
				if (cross_identityDocument.get("documentType").asText().equals("PASSPORT")) {
					cross_mappingValue = cross_identityDocument.get(cross_mappingEndTag).asText();
					break;
				}
			}
		}
		
		try {
			if (cross_mappingEndTag.equals("documentNumber")) {
				prove_mappingValue = proveIdRequest.getPassport().getPassportNumber();
			} else if (cross_mappingEndTag.equals("personalNumber")) {
				prove_mappingValue = proveIdRequest.getPassport().getPersonalNumber();
			} else if (cross_mappingEndTag.equals("nationalityCountryCode")) {
				prove_mappingValue = proveIdRequest.getPassport().getNationalityCode();
			} else if (cross_mappingEndTag.equals("issueCountryCode")) {
				prove_mappingValue = proveIdRequest.getPassport().getIssuingCountryCode();
			} else if (cross_mappingEndTag.equals("expiresDate")) {
				prove_mappingValue = proveIdRequest.getPassport().getExpiryDate().toString();
			} else if (cross_mappingEndTag.equals("MRZLine1")) {
				prove_mappingValue = proveIdRequest.getPassport().getPassportLine1();
			} else if (cross_mappingEndTag.equals("MRZLine2")) {
				prove_mappingValue = proveIdRequest.getPassport().getPassportLine2();
			}

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  	public boolean compareIdentityDrivingLicenseDocument(
			SearchType proveIdRequest, JsonNode crosscoreRootNode,
			String cross_mappingField, String prove_mappingField)
			throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_identityDocumentsArray;
		
		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_identityDocumentsArray = (JsonNode) cross_contactIterator.next().get("identityDocuments");
			for (JsonNode cross_identityDocument : cross_identityDocumentsArray) {
				if (cross_identityDocument.get("documentType").asText().equals("DRIVER_LICENSE")) {
					cross_mappingValue = cross_identityDocument.get(cross_mappingEndTag).asText();
					break;
				}
			}
		}
		
		try {
			if (cross_mappingEndTag.equals("documentNumber")) {
				prove_mappingValue = proveIdRequest.getDriverLicence().getDriverLicenceNumber();
			}

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  	public boolean compareIdentityPANDocument(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_identityDocumentsArray;
		List<IdentityDocumentType> prove_documentTypesList;
		
		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_identityDocumentsArray = (JsonNode) cross_contactIterator.next()
					.get("identityDocuments");
			for (JsonNode cross_identityDocument : cross_identityDocumentsArray) {
				if (cross_identityDocument.get("documentType").asText().equals("PAN")) {
					cross_mappingValue = cross_identityDocument.get(cross_mappingEndTag).asText();
					break;
				}
			}
		}
		
		try {
			prove_documentTypesList = proveIdRequest.getIdentityDocuments().getIdentityDocument();
			for (IdentityDocumentType prove_identityDocumentType : prove_documentTypesList) {
				if (prove_identityDocumentType.getTypeOfDocument().value().equals("PAN")) {
					if (cross_mappingEndTag.equals("documentType")) {
						prove_mappingValue = prove_identityDocumentType.getTypeOfDocument().value();
						break;
					} else if (cross_mappingEndTag.equals("documentNumber")) {
						prove_mappingValue = prove_identityDocumentType.getNumber().getValue();
						break;
					}
				}
			}

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

  	public boolean compareIdentitySINDocument(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_identityDocumentsArray;
		List<IdentityDocumentType> prove_documentTypesList;
		
		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_identityDocumentsArray = (JsonNode) cross_contactIterator.next().get("identityDocuments");
			for (JsonNode cross_identityDocument : cross_identityDocumentsArray) {
				if (cross_identityDocument.get("documentType").asText().equals("SIN")) {
					cross_mappingValue = cross_identityDocument.get(cross_mappingEndTag).asText();
					break;
				}
			}
		}
		
		try {
			prove_documentTypesList = proveIdRequest.getIdentityDocuments().getIdentityDocument();
			for (IdentityDocumentType prove_identityDocumentType : prove_documentTypesList) {
				if (prove_identityDocumentType.getTypeOfDocument().value().equals("SIN")) {
					if (cross_mappingEndTag.equals("documentType")) {
						prove_mappingValue = prove_identityDocumentType.getTypeOfDocument().value();
						break;
					} else if (cross_mappingEndTag.equals("documentNumber")) {
						prove_mappingValue = prove_identityDocumentType.getNumber().getValue();
						break;
					}
				}
			}

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}

	public boolean compareIdentityVoterIDDocument(
			SearchType proveIdRequest, JsonNode crosscoreRootNode,
			String cross_mappingField, String prove_mappingField)
			throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_identityDocumentsArray;
		List<IdentityDocumentType> prove_documentTypesList;
		
		String mappingCrossField = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_identityDocumentsArray = (JsonNode) cross_contactIterator.next().get("identityDocuments");
			for (JsonNode cross_identityDocument : cross_identityDocumentsArray) {
				if (cross_identityDocument.get("documentType").asText().equals("VOTER_ID")) {
					cross_mappingValue = cross_identityDocument.get(mappingCrossField).asText();
					break;
				}
			}
		}
		
		try {
			prove_documentTypesList = proveIdRequest.getIdentityDocuments().getIdentityDocument();
			for (IdentityDocumentType prove_identityDocumentType : prove_documentTypesList) {
				if (prove_identityDocumentType.getTypeOfDocument().value().equals("VoterID")) {
					if (mappingCrossField.equals("documentType")) {
						prove_mappingValue = "VOTER_ID";
						break;
					} else if (mappingCrossField.equals("documentNumber")) {
						prove_mappingValue = prove_identityDocumentType.getNumber().getValue();
						break;
					}
				}
			}

		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(prove_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
	}


	public boolean compareIdentitySSNDocument(SearchType proveIdRequest,
			JsonNode crosscoreRootNode, String cross_mappingField,
			String prove_mappingField) throws RequestMappingException {

		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_identityDocumentsArray;
		List<IdentityDocumentType> prove_documentTypesList;

		String cross_mappingEndTag = getCrossMappingEndTag(cross_mappingField);
		cross_contactIterator = crosscoreRootNode.path("payload").get("contacts").getElements();

		while (cross_contactIterator.hasNext()) {
			cross_identityDocumentsArray = (JsonNode) cross_contactIterator.next().get("identityDocuments");
			for (JsonNode cross_identityDocument : cross_identityDocumentsArray) {
				if (cross_identityDocument.get("documentType").asText().equals("SSN")) {
					cross_mappingValue = cross_identityDocument.get(cross_mappingEndTag).asText();
					break;
				}
			}
		}

		try {

			prove_documentTypesList = proveIdRequest.getIdentityDocuments().getIdentityDocument();
			for (IdentityDocumentType prove_identityDocumentType : prove_documentTypesList) {
				if (prove_identityDocumentType.getTypeOfDocument().value()
						.equals("SSN")) {
					if (cross_mappingEndTag.equals("documentType")) {
						prove_mappingValue = prove_identityDocumentType.getTypeOfDocument().value();
						break;
					} else if (cross_mappingEndTag.equals("documentNumber")) {
						prove_mappingValue = prove_identityDocumentType.getNumber().getValue();
						break;
					}
				}
			}
		} catch (NullPointerException e) {
			throw new RequestMappingException(prove_mappingField + " node value is null");
		}

		if (cross_mappingValue != null && prove_mappingValue != null && cross_mappingValue.equals(prove_mappingValue)) {
			isEqual = true;
		} else {
			throw new RequestMappingException(cross_mappingField + " doesn't match with proveid " + prove_mappingField);
		}

		return isEqual;
  }
  
  public boolean compareSecureCardTypes(SearchType proveIdRequest, JsonNode crosscoreReqJsonNode, String cross_value, String prove_value) throws RequestMappingException {

		boolean isEqual = false;

		Iterator<JsonNode> cross_contactIterator;
		JsonNode cross_cardArray;
		List<CardType> prove_cardList;
		List<OrderType> prove_orderList;

		cross_contactIterator = crosscoreReqJsonNode.path("payload").get("contacts").getElements();
		prove_orderList = proveIdRequest.getOrders().getOrder();

		while (cross_contactIterator.hasNext()) {

			cross_cardArray = (JsonNode) cross_contactIterator.next().get("cards");

			for (JsonNode card : cross_cardArray) {
				if (!card.path("brand").asText().equals(cross_value)) {
					throw new RequestMappingException(cross_value + " doesn't match with crosscore JSON request ");
				}
			}

		}

		for (OrderType prove_orderType : prove_orderList) {
			prove_cardList = prove_orderType.getPayment().getCards().getCard();

			try {

				for (CardType prove_cardType : prove_cardList) {

					if (prove_cardType.getType().equals(prove_value)) {
						isEqual = true;
						break;
					} else {
						throw new RequestMappingException(cross_value + " doesn't match with Prove Id card type called : " + prove_cardType.getType().toString());
					}

				}

			} catch (NullPointerException e) {
				throw new RequestMappingException(prove_value + " node value is null");
			}
		}


		return isEqual;
  }
  
  public boolean compareTelephoneTypes(SearchType proveIdRequest, JsonNode crosscoreRootNode,
			String cross_value, String prove_value) throws RequestMappingException {
	
		boolean isEqual = false;
		String cross_mappingValue = null;
		String prove_mappingValue = null;
	
		Iterator<JsonNode> cross_contactArray = null;
		JsonNode cross_telephoneArray = null;
		List<TelephoneType> prov_telephoneList = null;
		
		cross_contactArray = crosscoreRootNode.path("payload").get("contacts").getElements();
		
		while (cross_contactArray.hasNext()) {
			cross_telephoneArray = (JsonNode) cross_contactArray.next().get("telephones");
			for (JsonNode telephone : cross_telephoneArray) {
				cross_mappingValue = telephone.path("phoneIdentifier").asText();
			}
		}
		
		if (cross_value.equals(cross_mappingValue)) {
	
			prov_telephoneList = proveIdRequest.getTelephones().getTelephone();
	
			for (TelephoneType prov_telephone : prov_telephoneList) {
				try {
					prove_mappingValue = prov_telephone.getType();
				} catch (NullPointerException e) {
					throw new RequestMappingException("proveid telephone type node value is null");
				}
			}
	
			if (prove_value.equals(prove_mappingValue)) {
				isEqual = true;
			} else {
				throw new RequestMappingException(" PROVEID_VALUE: " + prove_value + " doesnot match with proveid telephone type: " + prove_mappingValue);
			}
		} else {
			throw new RequestMappingException("CROSSCORE_VALUE: " + cross_value+ " doesn't match with crosscore telephone type: " + cross_mappingValue);
		}
	
		return isEqual;
  }

  
  public String getCrossMappingEndTag(String crossMappingField) {

	  String crossMapping = crossMappingField.substring((crossMappingField.lastIndexOf(".") + 1));
	  return crossMapping;
  }


}
