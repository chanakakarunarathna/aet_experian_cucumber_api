package com.gfid.crosscore.mapper;

public class RequestMappingException extends Exception {

  private static final long serialVersionUID = 1L;

  public RequestMappingException(String message) {
    super("Mapping Error: "+message);
  }

}
