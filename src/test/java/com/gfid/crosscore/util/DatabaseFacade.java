package com.gfid.crosscore.util;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import xsd.search.SearchType;

public class DatabaseFacade {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public SearchType getProveIdXMLRequest(String clientReferenceId) {

		String proveidRequest = null;
		SearchType proveidRequestObj = null;
		List<Map<String, Object>> dbResults = null;

		try {

			logger.info("clientReferenceId is : " + clientReferenceId);
			
			dbResults = jdbcTemplate.queryForList("select * from dbo.Searches  where ClientRef='" + clientReferenceId + "'");
			
			for (Map<String, Object> result : dbResults) {
				proveidRequest = (String) result.get("SearchRequest");
			}
			proveidRequestObj = com.gfid.jaxb.JaxbHelper.getInstance().unmarshal(proveidRequest, SearchType.class);

		} catch (Exception e) {
			logger.error("Error while getting prove id request from database.");
		}
		return proveidRequestObj;
	}

}
