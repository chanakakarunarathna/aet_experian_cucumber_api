package com.gfid.crosscore.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CucumberUtil {

  private static final Logger logger = LoggerFactory.getLogger(CucumberUtil.class);

	/**
	 * Get formatted json request
	 * 
	 * @param String unformmatedJson
	 * @return String formattedJson
	 */
	public static String getFormattedJsonRequest(String unformmatedJson) {

		String formattedJson = unformmatedJson.replace("\r\n", "");
		return formattedJson;
	}

	/**
	 * Generate HMAC Key
	 * 
	 * @param String cross_formmatedJson
	 * @return String cross_hmacKey
	 */
	public static String generateHMAC(String cross_formmatedJson, String secretKey) {

		String cross_hmacKey = null;

		try {
			Mac mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
			mac.init(secretKeySpec);
			cross_hmacKey = Base64.encodeBase64String(mac.doFinal(cross_formmatedJson.getBytes()));

		} catch (Exception e) {
			logger.error("Error while generating HMAC");
		}

		return cross_hmacKey;
	}

	/**
	 * Generating formatted Date
	 *
	 * @return String formattedTime
	 */
	public static String generateFormattedDate() {

		SimpleDateFormat sdf = null;
		Timestamp timestamp = null;
		String formattedTime = null;

		try {
			sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			timestamp = new Timestamp(System.currentTimeMillis());
			formattedTime = sdf.format(timestamp);

		} catch (Exception e) {
			logger.error("Error while date formatting process");
		}
		return formattedTime;
	}

	/**
	 * Generate unique client reference id
	 * 
	 * @return String ClientReferenceId
	 */
	public static String generateClientReferenceId() {

		String ClientReferenceId = "crosscore_" + UUID.randomUUID();
		return ClientReferenceId;
	}

	/**
	 * Convert JSON file into String
	 * 
	 * @param String filePath
	 * @return String jsonString
	 */
	public static String readJsonFile(String filePath) {

		String jsonString = null;

		try {

			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			String line = null;
			StringBuilder stringBuilder = new StringBuilder();
			String ls = System.getProperty("line.separator");

			try {

				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append(ls);
				}
				jsonString = stringBuilder.toString();

			} finally {
				reader.close();
			}
		} catch (

		Exception e) {
			logger.error("Error while reading JSON file");
		}
		return jsonString;
	}

	/**
	 * Generate JSON root Node
	 * 
	 * @param String crosscoreRequest
	 * @return String JsonNode
	 */
	public static JsonNode getJsonRootNode(String crosscoreRequest) {

		ObjectMapper mapper = null;
		JsonNode rootNode = null;

		try {

			mapper = new ObjectMapper();
			rootNode = mapper.readTree(crosscoreRequest);

		} catch (IOException e) {
			logger.error("Error while generating JSON Root Node");
		}
		return rootNode;
	}

	/**
	 * Convert Crosscore expire date to prove id expire date
	 * 
	 * @param String  expireDate
	 * @return String expireDate
	 */
	public static String convertProveidTypeExpireDate(String expireDate) {
		
		String str1 = expireDate.substring(2, 4);
		String str2 = expireDate.substring(5, 7);

		return (str2 + str1);
	}

}
