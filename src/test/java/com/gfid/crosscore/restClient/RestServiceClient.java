package com.gfid.crosscore.restClient;

public interface RestServiceClient {
	
	public String sendCrosscoreRequest(String cross_jsonRequest, String cross_hmacKey);

}
