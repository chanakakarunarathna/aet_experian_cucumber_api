package com.gfid.crosscore.restClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestServiceClientImpl implements RestServiceClient {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private RestTemplate restTemplate;
	private String endpoint;
		
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String sendCrosscoreRequest(String cross_jsonRequest, String cross_hmacKey) {
		
		try{
			
			HttpHeaders headers = generateHeaders(cross_hmacKey);
			HttpEntity<String> entity = new HttpEntity<String>(cross_jsonRequest, headers);
			ResponseEntity<String> cross_response = restTemplate.exchange(endpoint,HttpMethod.POST, entity, String.class);
			
			return cross_response.getBody();
			
		}catch(Exception ex){
			logger.error("Error sending request Json --:" + ex.getMessage());
			return null;
		}

	}
	
	public HttpHeaders generateHeaders(String cross_hmacKey) {
		
		HttpHeaders headers=null;
		
		try{
			headers = new HttpHeaders();
			headers.set("hmac-signature", cross_hmacKey);
			headers.setContentType(MediaType.APPLICATION_JSON);
			
		}catch(Exception ex){
			logger.error("Error generating json headers --:" + ex.getMessage());
		}
		
		return headers;
	}
}