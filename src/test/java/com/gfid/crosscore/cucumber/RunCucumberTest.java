package com.gfid.crosscore.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"html:target/cucumber-report", "json:target/cucumber.json"}, tags = {"~@ignore"}, strict=true)
public class RunCucumberTest {
}

