package com.gfid.crosscore.cucumber;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.gfid.crosscore.mapper.RequestMapper;
import com.gfid.crosscore.mapper.RequestMappingException;
import com.gfid.crosscore.restClient.RestServiceClient;
import com.gfid.crosscore.util.CucumberUtil;
import com.gfid.crosscore.util.DatabaseFacade;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import xsd.search.SearchType;


public class Execution {

  private static final Logger logger = LoggerFactory.getLogger(Execution.class);
 
  @Autowired
  private RestServiceClient restServiceclient;

  @Autowired
  private DatabaseFacade databaseFacade;
  
  @Autowired
  private RequestMapper requestMapper;

  private String prop_username;
  private String prop_password;
  private String prop_secretKey;
  
  private String crosscoreReqJsonFile;
  private String crosscoreJsonRequest;
  private JsonNode crosscoreReqJsonNode;
  private String crosscoreJsonResponse;
  private SearchType proveIdRequest;
  List<Map<String, String>> mappingFiledHashes;

  	public Execution() {
		
		try {
			
			Properties props = new Properties();
			InputStream in = this.getClass().getResourceAsStream("/cucumber.properties");
			props.load(in);

			prop_username = props.getProperty("username");
			prop_password = props.getProperty("password");
			prop_secretKey = props.getProperty("hmac.secret");
			in.close();

		} catch (Exception e) {
			logger.error((e.getMessage()));
		}
	}

  	@Given("^A Valid (.*) User$")
	public void aValidProductUser(String product) {

	}

	@Given("^input the json file (.*) contained following values")
	public void inputTheJsonFile(String fileName, DataTable table) {
		this.crosscoreReqJsonFile = fileName;
	}

	@When("^The search is submitted$")
	public void theSearchIsSubmitted() throws UnsupportedEncodingException,
			JAXBException {

		String clientReferenceId = CucumberUtil.generateClientReferenceId();
		
		crosscoreJsonRequest = generateCrosscoreRequest(crosscoreReqJsonFile,clientReferenceId);
		crosscoreReqJsonNode = CucumberUtil.getJsonRootNode(crosscoreJsonRequest);
		crosscoreJsonResponse = sendCrosscoreRequest(crosscoreJsonRequest);
		
		proveIdRequest = databaseFacade.getProveIdXMLRequest(clientReferenceId);
		
	}
  
	@Then("^The system must map following sections$")
  	public void The_system_must_map_successful_request(DataTable table) throws RequestMappingException {
    
	  boolean result = false;
	  mappingFiledHashes = getHashes(table);

	  for (Map<String, String> hash : mappingFiledHashes) {
    	
	      if (hash.get("CROSSCORE_MAPPING").equals("clientReferenceId")) {
	    	  
	    	  result = requestMapper.compareClientReferenceId(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"),hash.get("PROVEID_MAPPING"));
	        
	      } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.person.personDetails")) {
	    	  
	    	  result = requestMapper.compareSinglePersonDetails(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"),hash.get("PROVEID_MAPPING"));
	        
	      } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.person")) {
	    	  
	    	  result = requestMapper.compareSinglePersonName(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"),hash.get("PROVEID_MAPPING"));
	        
	      } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.addresses")) {
	    	  
	    	  result = requestMapper.compareSingleAddress(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	        
	      } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.telephones")) {
	    	  
	    	  result = requestMapper.compareSingleTelephone(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	          
	      } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.emails")) {
	    	  
	    	  result = requestMapper.compareSingleEmail(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"),hash.get("CROSSCORE_MAPPING"));
	        
	      } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.identityDocuments.@[documentType=PASSPORT]")) {
	    	  
	          result = requestMapper.compareIdentityPassportDocument(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	          
		  } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.identityDocuments.@[documentType=DRIVER_LICENSE]")) {
			  
		      result = requestMapper.compareIdentityDrivingLicenseDocument(proveIdRequest,crosscoreReqJsonNode, hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
		      
		  } else if(hash.get("CROSSCORE_MAPPING").contains("cards")) {
	    	  
	    	  result = requestMapper.compareSingleCard(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"),hash.get("PROVEID_MAPPING"));
	        
	      }  else if (hash.get("CROSSCORE_MAPPING").contains("application.originalRequestTime")) {
	    	  
	    	  result = requestMapper.compareApplication(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	          
	      } else if (hash.get("CROSSCORE_MAPPING").contains("transactions")) {
	    	  
	    	  result = requestMapper.compareSingleTransaction(proveIdRequest, crosscoreReqJsonNode, hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	          
	      } else if(hash.get("CROSSCORE_MAPPING").contains("userDefinedFields")) {
	    	  
	    	  result = requestMapper.compareUserDefinedFields(proveIdRequest, crosscoreReqJsonNode, hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	    	
	      }  else if (hash.get("CROSSCORE_MAPPING").contains("contacts.identityDocuments.@[documentType=PAN]")) {
			  
		      result = requestMapper.compareIdentityPANDocument(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
		      
		  } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.identityDocuments.@[documentType=SSN]")) {
			  
		      result = requestMapper.compareIdentitySSNDocument(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
		      
		  } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.identityDocuments.@[documentType=VoterID]")) {
			  
		      result = requestMapper.compareIdentityVoterIDDocument(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
		      
		  } else if (hash.get("CROSSCORE_MAPPING").contains("contacts.identityDocuments.@[documentType=SIN]")) {
			  
		      result = requestMapper.compareIdentitySINDocument(proveIdRequest, crosscoreReqJsonNode, hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
		      
		  } else if (hash.get("CROSSCORE_MAPPING").contains("control.option")) {
	    	  
	    	  result = requestMapper.compareControlOptions(proveIdRequest, crosscoreReqJsonNode,hash.get("CROSSCORE_MAPPING"), hash.get("PROVEID_MAPPING"));
	        
	      }
      
	      if (!result) {
  			break;
	      }
	  }
    
	  assertTrue(result);
  	}
  
  	@Then("^The system must map multiple (.*)")
	public void The_system_must_map_multiple_values(String cross_mapping,
			List<String> valueList) throws RequestMappingException {

		boolean result = false;

		if (cross_mapping.contains("contacts.telephones")) {
			result = requestMapper.compareMultiTelephone(proveIdRequest,crosscoreReqJsonNode, valueList, cross_mapping);
		} else if (cross_mapping.equals("contacts.emails")) {
			result = requestMapper.compareMultipleEmails(proveIdRequest,crosscoreReqJsonNode, valueList);
		}

		assertTrue(result);
	}
  
  	@Then("The system must map following (.*) values")
	public void The_system_must_map_following_values(String cross_mapping,
			DataTable table) throws RequestMappingException {

		boolean result = false;
		mappingFiledHashes = getHashes(table);

		if (cross_mapping.contains("cards.brand")) {
			for (Map<String, String> hash : mappingFiledHashes) {
				result = requestMapper.compareSecureCardTypes(proveIdRequest, crosscoreReqJsonNode, hash.get("CROSSCORE_VALUE"),hash.get("PROVEID_VALUE"));
			}
		} else if (cross_mapping
				.contains("contacts.telephones.phoneIdentifier")) {
			for (Map<String, String> hash : mappingFiledHashes) {
				result = requestMapper.compareTelephoneTypes(proveIdRequest, crosscoreReqJsonNode, hash.get("CROSSCORE_VALUE"),hash.get("PROVEID_VALUE"));
			}
		}

		assertTrue(result);
	}
  
  	private String generateCrosscoreRequest(String cross_requestJsonFile, String clientReferenceId) {

		String cross_requestJson = null;

		try {

			cross_requestJson = CucumberUtil.readJsonFile(new File("").getAbsolutePath() + "\\src\\test\\resources\\json-files\\" + cross_requestJsonFile);
			cross_requestJson = cross_requestJson.replaceAll("##clientReferenceId##", clientReferenceId);
			cross_requestJson = cross_requestJson.replaceAll("##now##", CucumberUtil.generateFormattedDate());

		} catch (Exception e) {
			logger.error("Exception while generating request JSON with current Time & clientReferenceId :" + e.getMessage());
		}

		return cross_requestJson;
	}
  	
  	private String sendCrosscoreRequest(String cross_jsonRequest) {
  		
  		String cross_formattedJsonRequest = CucumberUtil.getFormattedJsonRequest(cross_jsonRequest);
		String cross_hmacKey = CucumberUtil.generateHMAC(cross_formattedJsonRequest, prop_secretKey);
		String cross_jsonResponse = restServiceclient.sendCrosscoreRequest(cross_formattedJsonRequest, cross_hmacKey);
		
  	    logger.info("Get crosscore response : " + cross_jsonResponse);
  	    return cross_jsonResponse;
  	}
  	
  	
  	private List<Map<String, String>> getHashes(DataTable table) {
    
  		List<Map<String, String>> hashes = new ArrayList<Map<String, String>>();
  		List<List<String>> rows = table.raw();
  		List<String> headers = rows.get(0);

  		for (int rowIndex = 1; rowIndex < rows.size(); ++rowIndex) {
      
		  Map<String, String> rowHash = new HashMap<String, String>();
		  List<String> values = rows.get(rowIndex);

	      for (int columnIndex = 0; columnIndex < headers.size(); ++columnIndex) {
	
	    	  rowHash.put(headers.get(columnIndex), values.get(columnIndex));
	      }
	      
	      hashes.add(rowHash);
  		}

  		return hashes;
  	}
  

}
