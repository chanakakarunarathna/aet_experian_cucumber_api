@S00005
Feature: UK ProveId - Passport details mapping
      
      Scenario: Mapping Passport details
    	Given A Valid CrossCore User
			And input the json file S00005_GBR_Passport.json contained following values
  		|TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH   |GENDER 	|BUILDING_NUMBER	|STREET			|POSTTOWN					|COUNTRY	|POSTAL		|COUNTRY_CODE|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      |M				|345							|Blea Beck	|Askam-In-Furness	|Cumbria	|LA16 7DG	|GBR				 |
   	 	When The search is submitted
			Then The system must map following sections
			|CROSSCORE_MAPPING											  																	|PROVEID_MAPPING						|
			|contacts.identityDocuments.@[documentType=PASSPORT].documentNumber 				|Passport.PassportNumber		|
			|contacts.identityDocuments.@[documentType=PASSPORT].personalNumber					|Passport.PersonalNumber		|
			|contacts.identityDocuments.@[documentType=PASSPORT].nationalityCountryCode	|Passport.NationalityCode		|
			|contacts.identityDocuments.@[documentType=PASSPORT].issueCountryCode				|Passport.IssuingCountryCode|
			|contacts.identityDocuments.@[documentType=PASSPORT].expiresDate						|Passport.ExpiryDate				|
			|contacts.identityDocuments.@[documentType=PASSPORT].MRZLine1								|Passport.PassportLine1			|
			|contacts.identityDocuments.@[documentType=PASSPORT].MRZLine2								|Passport.PassportLine2			|