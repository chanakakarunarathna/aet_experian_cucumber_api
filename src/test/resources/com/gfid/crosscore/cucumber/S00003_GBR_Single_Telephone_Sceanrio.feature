@S00003
Feature: UK ProveId - Single telephone details mappings
			Scenario: Mapping single telephone details 
    	Given A Valid CrossCore User
      And input the json file S00003_GBR_Single_Telephone.json contained following values
      |TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH     |GENDER 	|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      	 |M				|
      When The search is submitted
      Then The system must map following sections
      |CROSSCORE_MAPPING         								|PROVEID_MAPPING            |
      |contacts.telephones.phoneIdentifier			|Telephones.Telephone Type	|
      |contacts.telephones.number								|Telephones.Number					|
      
      
      
      
			
			