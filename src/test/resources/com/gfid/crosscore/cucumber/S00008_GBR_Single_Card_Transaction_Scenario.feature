@S00008
Feature: UK ProveId - Single card transaction mapping
        
      Scenario: Mapping card transaction details
    	Given A Valid CrossCore User
    	And input the json file S00008_GBR_Single_Card_Transaction.json contained following values
	    |TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH   |GENDER 	|BUILDING_NUMBER	|STREET			|POSTTOWN					|COUNTRY	|POSTAL		|COUNTRY_CODE|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      |M				|345							|Blea Beck	|Askam-In-Furness	|Cumbria	|LA16 7DG	|GBR				 |
    	When The search is submitted
      Then The system must map following sections
      |CROSSCORE_MAPPING                            |PROVEID_MAPPING                                                  									|
      |application.originalRequestTime     					|Orders.Order.Payment.Cards.Card.Transaction.TransactionDate												|
      |transactions.cashValue.amount                |Orders.Order.Payment.Cards.Card.Transaction.TransactionAmount.Amount								|
      |transactions.cashValue.currencyCode          |Orders.Order.Payment.Cards.Card.Transaction.TransactionAmount.Currency							|
     
      
      
