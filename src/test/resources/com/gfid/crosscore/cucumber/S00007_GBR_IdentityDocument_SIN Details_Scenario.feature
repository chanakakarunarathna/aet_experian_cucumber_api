@S00007
Feature: UK ProveId - Identitiy document - SIN details mapping
  
  Scenario: Mapping IdentityDocument - SIN Details
    Given A Valid CrossCore User
    And input the json file S00007_GBR_IdentityDocument_SIN Details.json contained following values
      | TITLE | FORENAME | SURNAME | DATE_OF_BIRTH | GENDER | BUILDING_NUMBER | STREET    | POSTTOWN         | COUNTRY | POSTAL   | COUNTRY_CODE |
      | MR    | SIRATH   | NIVER   | 1994-07-01    | M      |             345 | Blea Beck | Askam-In-Furness | Cumbria | LA16 7DG | GBR          |
    When The search is submitted
    Then The system must map following sections
      | CROSSCORE_MAPPING                                             | PROVEID_MAPPING                 |
      | contacts.identityDocuments.@[documentType=SIN].documentType   | IdentityDocument.TypeOfDocument |
      | contacts.identityDocuments.@[documentType=SIN].documentNumber | IdentityDocument.Number         |
