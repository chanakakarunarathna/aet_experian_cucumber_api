@S00009
Feature: UK ProveId - Single card mappings
    
    Scenario: Mapping clear_card number - verify card number is in correct format
    Given A Valid CrossCore User
		And input the json file S00009_GBR_Single_Card_Security_Code.json contained following values
    | COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
    | GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC  |
    When The search is submitted
    Then The system must map following sections
    |CROSSCORE_MAPPING       |PROVEID_MAPPING  |  
    |cards.clearCardNumber   |Orders.Order.Payment.Cards.Card.Number|
    
    
    Scenario: Mapping all card details
    Given A Valid CrossCore User
	  And input the json file S00009_GBR_Single_Card_Security_Code.json contained following values
    | COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
    | GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC  |
    When The search is submitted
    Then The system must map following sections
		|CROSSCORE_MAPPING								|PROVEID_MAPPING														|
		|cards.brand											|Orders.Order.Payment.Cards.Type						|
		|cards.cardBinNumber							|Orders.Order.Payment.Cards.BINNumber				|
		|cards.clearCardNumber						|Orders.Order.Payment.Cards.Number					|
		|cards.cardLast4Digits						|Orders.Order.Payment.Cards.CardLast4Digits |
		|cards.cardHolderName							|Orders.Order.Payment.Cards.Name						|
		|cards.expireDate									|Orders.Order.Payment.Cards.ExpiresEnd 			|
		|cards.startDate									|Orders.Order.Payment.Cards.StartDate 			|
		|cards.issueNumber								|Orders.Order.Payment.Cards.IssueNumber 		|
		|cards.securityCode							  |Orders.Order.Payment.Cards.SecurityCode 		|
    