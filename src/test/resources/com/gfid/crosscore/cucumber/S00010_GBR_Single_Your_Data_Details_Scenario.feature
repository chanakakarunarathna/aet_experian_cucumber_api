@ignore
@S00010

Feature: UK ProveId - Your_data details mapping
    #Proper data is not available to check the implementation(From ProveId)
   
    Scenario: Mapping YourData Details
    Given A Valid CrossCore User
		And input the json file S00010_GBR_Your_Data_Details.json contained following values
    | COUNTRY_CODE | FORENAME | SURNAME | PREMISE | POSTCODE | PRODUCT_CODE |
    | GBR          | SIRATH   | NIVER   | 345     | LA16 7DG | ProveID_KYC |
    When The search is submitted
    Then The system must map following sections
    |CROSSCORE_MAPPING           |PROVEID_MAPPING						|  
    |userDefinedFields.fieldName |YourData.Fields.Field Name|
	  |userDefinedFields.fieldValue|YourData.Fields.Value			|
