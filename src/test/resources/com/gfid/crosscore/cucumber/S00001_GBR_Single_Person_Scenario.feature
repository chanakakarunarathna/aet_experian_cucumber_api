@S00001
Feature: UK ProveId - Single Person mappings
			
			Scenario: Crosscore 'clientReferenceId' mapping with Proveid 'YourReference'
			Given A Valid CrossCore User
			And input the json file S00001_GBR_Single_Person.json contained following values 
  		|TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH     |GENDER 	|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      	 |M				|
			When The search is submitted
			Then The system must map following sections
			|CROSSCORE_MAPPING				|PROVEID_MAPPING			|
			|clientReferenceId				|YourReference				|
			

			Scenario: Crosscore 'person' mapping with Proveid 'person' 
			Given A Valid CrossCore User
	  	And input the json file S00001_GBR_Single_Person.json contained following values 
  		|TITLE	| FORENAME | SURNAME 	|DATE_OF_BIRTH   |GENDER 	|
    	|MR		 	| SIRATH   | NIVER    |1994-07-01      |M				|
			When The search is submitted
			Then The system must map following sections
			|CROSSCORE_MAPPING													|PROVEID_MAPPING				|
			|contacts.person.personDetails.dateOfBirth	|Person.DateOfBirth			|
			|contacts.person.personDetails.gender				|Person.Gender   				|
			|contacts.person.names.title								|Person.Name.Title			|
			|contacts.person.names.firstName						|Person.Name.Forename 	|
			|contacts.person.names.middleNames					|Person.Name.OtherNames	|
			|contacts.person.names.surName							|Person.Name.Surname  	|
			
				 		