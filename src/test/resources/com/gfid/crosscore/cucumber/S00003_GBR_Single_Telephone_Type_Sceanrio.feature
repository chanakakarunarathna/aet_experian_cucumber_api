@S00003
Feature: UK ProveId - Single Telephone types mappings

  	Scenario: Mapping Home Telephone Type
    Given A Valid CrossCore User
    And input the json file S00003_GBR_Single_Telephone_Type_HOME.json contained following values
      | TITLE | FORENAME | SURNAME | DATE_OF_BIRTH | GENDER |
      | MR    | SIRATH   | NIVER   | 1994-07-01    | M      |
    When The search is submitted
    Then The system must map following contacts.telephones.phoneIdentifier values
      | CROSSCORE_VALUE | PROVEID_VALUE |
      | HOME            | H             |

  	Scenario: Mapping WORK Telephone Type
    Given A Valid CrossCore User
    And input the json file S00003_GBR_Single_Telephone_Type_WORK.json contained following values
      | TITLE | FORENAME | SURNAME | DATE_OF_BIRTH | GENDER |
      | MR    | SIRATH   | NIVER   | 1994-07-01    | M      |
    When The search is submitted
    Then The system must map following contacts.telephones.phoneIdentifier values
      | CROSSCORE_VALUE | PROVEID_VALUE |
      | WORK            | W             |

  	Scenario: Mapping LANDLINE Telephone Type
    Given A Valid CrossCore User
    And input the json file S00003_GBR_Single_Telephone_Type_LANDLINE.json contained following values
      | TITLE | FORENAME | SURNAME | DATE_OF_BIRTH | GENDER |
      | MR    | SIRATH   | NIVER   | 1994-07-01    | M      |
    When The search is submitted
    Then The system must map following contacts.telephones.phoneIdentifier values
      | CROSSCORE_VALUE | PROVEID_VALUE |
      | LANDLINE        | L             |

  	Scenario: Mapping MOBILE Telephone Type
    Given A Valid CrossCore User
    And input the json file S00003_GBR_Single_Telephone_Type_MOBILE.json contained following values
      | TITLE | FORENAME | SURNAME | DATE_OF_BIRTH | GENDER |
      | MR    | SIRATH   | NIVER   | 1994-07-01    | M      |
    When The search is submitted
    Then The system must map following contacts.telephones.phoneIdentifier values
      | CROSSCORE_VALUE | PROVEID_VALUE |
      | MOBILE          | M             |
